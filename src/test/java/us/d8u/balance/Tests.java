package us.d8u.balance;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class Tests {
	private OkHttpClient client = new OkHttpClient();

	@LocalServerPort
	private int port;

	@Autowired
	UserRepositoryImpl repo;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	public JavaMailSender emailSender;

	private static Logger LOG = LoggerFactory.getLogger("us.d8u.balance.Tests");

	private Map<String, String> fetchFromKeyserverReturnsValidKey(String param)
			throws Exception {

		Map<String, String> response = PGPUtils.fetchFromKeyserver(param);
		return response;
	}

	@AfterClass
	// FIXME use in-memory h2 database for testing
	public static void tearDown() throws Exception {

		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime
				.exec("/usr/local/bin/heroku pg:killall -a hd1-units");
		proc.waitFor();
	}

	@Test
	public void pomXmlUrlReturnsJson() throws Exception {
		String expectedJson = "{\"groupId\":\"us.d8u\",\"artifactId\":\"here\",\"packaging\":\"jar\",\"version\":\"1.0-SNAPSHOT\",\"url\":\"http://maven.apache.org\",\"dependencies\":\"[{\\\"groupId\\\":\\\"com.squareup.okhttp\\\",\\\"scope\\\":\\\"test\\\",\\\"artifactId\\\":\\\"okhttp\\\",\\\"type\\\":\\\"jar\\\",\\\"version\\\":\\\"2.5.0\\\"},{\\\"groupId\\\":\\\"org.junit.jupiter\\\",\\\"scope\\\":\\\"test\\\",\\\"artifactId\\\":\\\"junit-jupiter-engine\\\",\\\"type\\\":\\\"jar\\\",\\\"version\\\":\\\"5.5.2\\\"}]\"}";

		String response = this.restTemplate.getForObject("/maven/?url=https://raw.githubusercontent.com/hasandiwan/here/master/pom.xml", String.class);
		Map<String, String> actual = new Gson().fromJson(response, new TypeToken<Map<String, String>>() {}.getType());
		Map<String, String> expected = new Gson().fromJson(expectedJson, new TypeToken<Map<String, String>>() {}.getType());
		Assert.assertEquals(actual.entrySet(), expected.entrySet());
	}

	@Test
	public void twoDollarsEqualsTwoDollars() throws Exception {

		String rawResponse = this.restTemplate
				.getForObject("/money/2?from=USD&to=USD", String.class);
		Map<String, String> response = new Gson().fromJson(rawResponse,
				new TypeToken<Map<String, String>>() {
		}.getType());
		LocalDate today = LocalDate.now();
		DateTimeFormatter fmt = DateTimeFormatter.ISO_LOCAL_DATE;
		String timestamp = fmt.format(today);
		Assert.assertEquals("2", response.get("amount"));
		Assert.assertEquals("USD", response.get("sourceCurrency"));
		Assert.assertEquals("USD", response.get("destinationCurrency"));
		Assert.assertEquals(timestamp, response.get("requestedDate"));
	}

	@Test
	public void moneyReturnsErrorForInvalidSynbols() {

		ResponseEntity<String> rawResponse = this.restTemplate
				.getForEntity("/money/2?from=UnitY&to=USD", String.class);
		Map<String, String> response = new Gson().fromJson(
				rawResponse.getBody(), new TypeToken<Map<String, String>>() {
				}.getType());
		Assert.assertTrue(response.get("error")
				.contains("currency not found; valid keys are"));
		Assert.assertEquals(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS,
				rawResponse.getStatusCode());

	}

	@Test
	public void redditKeysAreLowerCamelCase() throws Exception {

		Request request = new Request.Builder()
				.url(new HttpUrl.Builder().host("localhost").port(this.port)
						.scheme("http").addPathSegment("reddit").build())
				.get().build();
		Response res = null;
		try {
			res = this.client.newCall(request).execute();
		} catch (IOException e) {
			Tests.LOG.error(e.getMessage(), e);
		}
		List<Map<String, String>> response = new Gson().fromJson(
				res.body().string(),
				new TypeToken<List<Map<String, String>>>() {
				}.getType());
		for (String k : response.get(0).keySet()) {
			Assert.assertFalse(k.contains("_"));
			Assert.assertTrue(k.substring(0, 1).toLowerCase()
					.equals(k.subSequence(0, 1)));
		}
	}

	@Test
	public void plusEncodingOsmUrlsWorks() throws Exception {

		Request request = new Request.Builder().url(new HttpUrl.Builder()
				.host("localhost").port(this.port).scheme("http")
				.addPathSegment("plus")
				.addQueryParameter("url",
						"https://www.openstreetmap.org/#map=18/34.10568/-118.44618")
				.build()).build();
		Response resp = this.client.newCall(request).execute();
		Map<String, String> response = new Gson().fromJson(resp.body().string(),
				new TypeToken<Map<String, String>>() {
		}.getType());
		Assert.assertEquals("85634H43+7G", response.get("plusCode"));
	}

	@Test
	public void nonexistentUserReturnsNotFoundAndSentinelAmountForBalance() {

		Request request = new Request.Builder()
				.url(new HttpUrl.Builder().host("localhost").port(this.port)
						.scheme("http").addPathSegment("bal")
						.addQueryParameter("user", "a@mailinator.com").build())
				.get().build();
		Response res = null;
		try {
			res = this.client.newCall(request).execute();
		} catch (IOException e) {
			Tests.LOG.error(e.getMessage(), e);
		}
		Map<String, BigDecimal> response = null;
		try {
			response = new Gson().fromJson(res.body().string(),
					new TypeToken<Map<String, BigDecimal>>() {
			}.getType());
		} catch (JsonSyntaxException e) {
			Tests.LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			Tests.LOG.error(e.getMessage(), e);
		}
		List<String> keys = new ArrayList<>(response.keySet());
		Assert.assertTrue(keys.get(0).endsWith("not found"));
		Assert.assertEquals("-999",
				response.get("a@mailinator.com not found").toPlainString());
		Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				res.code());
	}

	@Test
	public void geocodeRequiresAddressOrLatLon() throws Exception {

		Map<String, String> responseComponent = new StringMap();
		responseComponent.put("error", "specify both lat and lng **or** addr");
		List<Map<String, String>> expectedResponse = Collections
				.singletonList(responseComponent);
		String expected = new Gson().toJson(expectedResponse);
		RequestBody body = RequestBody.create(
				MediaType.parse("application/json"),
				new Gson().toJson(expected));
		Request request = new Request.Builder()
				.url(new HttpUrl.Builder().host("localhost").scheme("http")
						.port(this.port).addPathSegment("geocode").build())
				.post(body).build();
		Response response = this.client.newCall(request).execute();
		Assert.assertTrue(!response.isSuccessful());
	}

	@Test
	public void plusEncodingRequiresCodeOrLocationOrUrl() throws Exception {

		String message = "location, latitude/longitude, url, or code required as body";
		Map<String, String> incoming = Collections.singletonMap("error",
				message);
		RequestBody body = RequestBody.create(
				MediaType.parse("application/json"),
				new Gson().toJson(incoming));
		Request request = new Request.Builder()
				.url(new HttpUrl.Builder().scheme("http").host("localhost")
						.port(this.port).addPathSegment("plus").build())
				.post(body).build();
		Response response = this.client.newCall(request).execute();
		Assert.assertEquals(response.code(),
				HttpStatus.PRECONDITION_REQUIRED.value());
		Assert.assertEquals(message, new Gson().fromJson(
				response.body().string(), new TypeToken<Map<String, String>>() {
				}.getType()));
	}

	@Test
	public void reversePlusEncodingWorks() throws Exception {

		StringMap expected = new Gson().fromJson(
				"{\"request\":\"849VRH2X+68\",\"latitude\":\"37.8005625\",\"radius\":\"1.7677669530250526E-4\",\"longitude\":\"-122.40168750000001\"} ",
				StringMap.class);
		StringMap actual = this.restTemplate.postForObject("/plus",
				Collections.singletonMap("code", "849VRH2X+68"),
				StringMap.class);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void plusEncodingLatLonWorks() throws Exception {

		Map<String, String> expected = new HashMap<>();
		expected.put("plusCode", "85634H42+HH");
		Map<String, String> params = new HashMap<>();
		params.put("latitude", "34.10638");
		params.put("longitude", "-118.44858");
		expected.putAll(params);
		Map<String, String> actual = this.restTemplate.postForObject("/plus",
				params, StringMap.class);
		Assert.assertEquals(expected.keySet(), actual.keySet());
	}

	@Test
	public void plusEncodingLocationWorks() throws Exception {

		StringMap expected = new StringMap();
		expected.put("request",
				"2017 North Beverly Glen Blvd, Los Angeles, CA 90077, USA");
		expected.put("plusCode", "85634H43+7G");
		StringMap actual = this.restTemplate.postForObject("/plus",
				Collections.singletonMap("location",
						"2017 North Beverly Glen Blvd, Los Angeles, CA 90077, USA"),
				StringMap.class);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void forwardGeocodeWorks() throws Exception {

		List<StringMap> expected = new Gson().fromJson(
				"[{\"address\":\"Emerging Market Services, 2017, North Beverly Glen Boulevard, Beverly Glen, Westwood, Los Angeles, Los Angeles County, California, 90077, United States of America\",\"request\":\"(34.1056855, -118.4461788)\"}]",
				new TypeToken<List<StringMap>>() {
				}.getType());
		String paramsJson = "{\"lat\": \"34.1056855\", \"lng\": \"-118.4461788\"}";
		RequestBody body = RequestBody
				.create(MediaType.parse("application/json"), paramsJson);
		HttpUrl requestingUrl = new HttpUrl.Builder().scheme("http")
				.host("localhost").port(this.port).addPathSegment("geocode")
				.build();
		Request request = new Request.Builder().url(requestingUrl).post(body)
				.build();
		List<StringMap> actual = new Gson().fromJson(
				this.client.newCall(request).execute().body().string(),
				new TypeToken<List<StringMap>>() {
				}.getType());
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void revserseGeocodeWorks() throws Exception {

		List<Map<String, String>> expected = new Gson().fromJson(
				"[{\"address\":\"82, La Rambla, el Gòtic, Ciutat Vella, Barcelona, Barcelonès, Barcelona, Catalunya, 80002, España\",\"request\":\"(41.3813934, 2.1730637)\"}]",
				new TypeToken<List<Map<String, String>>>() {
				}.getType());
		Map<String, String> params = new HashMap<>();
		params.put("lat", "41.3813934");
		params.put("lng", "2.1730637");
		RequestBody body = RequestBody.create(
				MediaType.parse("application/json"), new Gson().toJson(params));
		Request request = new Request.Builder()
				.url(new HttpUrl.Builder().scheme("http").host("localhost")
						.port(this.port).addPathSegment("geocode").build())
				.post(body).build();
		Response resp = this.client.newCall(request).execute();
		List<Map<String, String>> actual = new Gson().fromJson(
				resp.body().string(),
				new TypeToken<List<Map<String, String>>>() {
				}.getType());
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void fetchValidKeyFromKeyserver() throws Exception {

		Map<String, String> actual = this
				.fetchFromKeyserverReturnsValidKey("hasandiwan@gmail.com");
		Assert.assertTrue(actual.containsKey("fingerprint"));
		Assert.assertTrue(actual.containsKey("keyId"));
		Assert.assertTrue(actual.containsKey("key"));
		Assert.assertTrue(actual.containsKey("request"));
	}

	@Test
	public void fetchInvalidKeyFromKeyserver() throws Exception {

		Map<String, String> actual = this
				.fetchFromKeyserverReturnsValidKey("hasan.diwan@gmail.com");
		Assert.assertTrue(actual.containsKey("error"));
		Assert.assertEquals(actual.get("error"), "Key not found");
		Assert.assertTrue(actual.containsKey("request"));
		Assert.assertEquals(actual.get("request"), "hasan.diwan@gmail.com");
	}

	@Test
	public void fetchKeyFromKeyserverUsingFingerprint() throws Exception {

		this.fetchFromKeyserverReturnsValidKey(
				"44D0 5643 E391 4542 7702  E1E7 FEBA D7FF D041 BBA1");
	}

	@Test
	public void invalidPageRaisesExpectationFailed() throws Exception {

		String url = "http://localhost:" + this.port + "/json?page=foo";
		Request request = new Request.Builder().url(url).build();
		Response resp = this.client.newCall(request).execute();
		List<List<Map<String, String>>> expected = Collections
				.singletonList(Collections.singletonList(Collections
						.singletonMap("error", "invalid url -- foo")));
		List<List<Map<String, String>>> actual = new Gson().fromJson(
				resp.body().string(),
				new TypeToken<List<List<Map<String, String>>>>() {
				}.getType());
		Assert.assertEquals(HttpStatus.SERVICE_UNAVAILABLE.value(),
				resp.code());
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void missingPageReturnsPreconditionFailed() throws Exception {

		ResponseEntity<List<List<Map<String, String>>>> response = this.restTemplate
				.getForEntity("/json", null);
		List<List<Map<String, String>>> expected = Collections.singletonList(
				Collections.singletonList(Collections.singletonMap("error",
						"no way to figure out what you want if you don't give a page")));
		Assert.assertEquals(expected, response.getBody());
		Assert.assertEquals(HttpStatus.PRECONDITION_FAILED,
				response.getStatusCode());
	}

	public void htmlTableParsesProperly() throws Exception {

		List<List<Map<String, String>>> expectedSrc = new ArrayList<>();

		List<Map<String, String>> tbls = new ArrayList<>();
		Map<String, String> tbl = new HashMap<>();
		tbl.put("city", "Barcelona");
		tbl.put("visited", "true");
		tbls.add(tbl);
		tbl = new HashMap<>();
		tbl.put("city", "Copenhagen");
		tbl.put("visited", "false");
		tbls.add(tbl);
		tbl = new HashMap<>();
		tbl.put("city", "Los Angeles");
		tbl.put("visited", "true");
		tbls.add(tbl);
		expectedSrc.add(tbls);

		ResponseEntity<String> response = this.restTemplate.getForEntity(
				"/json?page=http://localhost:" + this.port + "/test.html",
				String.class);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		String jsonBody = response.getBody();
		List<List<Map<String, String>>> actual = new Gson().fromJson(jsonBody,
				new TypeToken<List<List<Map<String, String>>>>() {
		}.getType());
		Assert.assertEquals(expectedSrc, actual);
	}

	@Test
	public void icmpWithPort() throws Exception {

		String url = "http://localhost:" + this.port + "/icmp/google.com";
		Request request = new Request.Builder().url(url).build();
		Response response = this.client.newCall(request).execute();
		Map<String, String> actual = new Gson().fromJson(
				response.body().string(), new TypeToken<Map<String, String>>() {
				}.getType());
		Map<String, String> expected = new HashMap<>();
		expected.put("host", "google.com");
		expected.put("ssl", "true");
		expected.put("path", "/");
		expected.put("reachable", "true");
		Assert.assertTrue(response.isSuccessful());
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void icmpWithPortNoSSL() throws Exception {

		HttpUrl endpoint = new HttpUrl.Builder().addPathSegment("icmp")
				.addPathSegment("purple.com").port(this.port)
				.addQueryParameter("port",
						Integer.valueOf(this.port).toString())
				.scheme("http").host("localhost").build();
		Request request = new Request.Builder().get().url(endpoint).build();
		Response response = this.client.newCall(request).execute();
		Map<String, String> expected = new HashMap<>();
		expected.put("host", "purple.com");
		expected.put("port", Integer.toString(this.port));
		expected.put("path", "/");
		expected.put("ssl", "false");
		expected.put("reachable", "true");
		Map<String, String> actual = new Gson().fromJson(
				response.body().string(), new TypeToken<Map<String, String>>() {
				}.getType());

		Assert.assertTrue(response.isSuccessful());
		Assert.assertEquals(expected, actual);
	}

}
