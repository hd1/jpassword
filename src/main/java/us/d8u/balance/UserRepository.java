package us.d8u.balance;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository
extends JpaRepository<User, Long>, CustomUserRepository {
	User findByName(String name);
	User findByNumber(Long number);
	User findByTelegramUserId(String id);
	User findByAccessKeyAndAccessSecret(String accessKey, String accessSecret);

}
