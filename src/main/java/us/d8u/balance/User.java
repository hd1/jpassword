package us.d8u.balance;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.validator.routines.EmailValidator;

@Entity(name="users")
public class User {
	@Column(unique = true)
	private String name;

	@Column
	private String telegramUserId;

	@Column
	private String accessKey;

	@Column
	private String accessSecret;

	@Column
	private BigDecimal moneyAmount = BigDecimal.ONE.multiply(new BigDecimal(5));

	@Column
	private Locale locale;

	@Column
	private Long number;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long userId;

	public User() {

		this.moneyAmount = new BigDecimal(
				.45);
		this.accessKey = UUID.randomUUID().toString();
		this.accessSecret = UUID.randomUUID().toString();
		this.locale = Locale.getDefault();
	}

	public User(String string, Long number, Locale string3) {

		BigDecimal amount = new BigDecimal(
				.45);
		this.setName(string);
		this.setNumber(number);
		this.moneyAmount = amount;
		this.setLocale(string3);
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((this.name == null) ? 0 : this.name.hashCode());
		result = (prime * result)
				+ ((this.userId == null) ? 0 : this.userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (this.getClass() != obj.getClass()) { return false; }
		User other = (User) obj;
		if (this.name == null) {
			if (other.name != null) { return false; }
		} else
			if (!this.name.equals(other.name)) { return false; }
		return true;
	}

	public Long getUserId() {

		return this.userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public String getName() {

		return this.name;
	}

	public void setName(String name) {

		if (EmailValidator.getInstance().isValid(name)) {
			this.name = name;
		}
	}

	public String getAccessKey() {

		return this.accessKey;
	}

	public void setAccessKey(String token) {

		this.accessKey = token;
	}

	public BigDecimal getMoneyAmount() {

		return this.moneyAmount;
	}

	public void setMoneyAmount(BigDecimal bigMoney) {
		this.moneyAmount = bigMoney;
	}

	public Long getNumber() {

		return this.number;
	}

	public void setNumber(Long number) {

		this.number = number;
	}

	public String getMoneySymbol() {

		return Currency.getInstance(this.locale).getSymbol();
	}

	public void setLocale(Locale locale) {

		this.locale = locale;
	}

	public Locale getLocale() {

		return this.locale;
	}

	public String getTelegramUserId() {

		return this.telegramUserId;
	}

	public void setTelegramUserId(String telegramUserId) {

		this.telegramUserId = telegramUserId;
	}

	public String getAccessSecret() {

		return this.accessSecret;
	}

	public void setAccessSecret(String accessSecret) {

		this.accessSecret = accessSecret;
	}

}
