package us.d8u.balance;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EndpointRepository extends JpaRepository<Endpoint, Long> {

	Endpoint findByEndpoint(String endpoint);

}
