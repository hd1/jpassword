package us.d8u.balance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.net.whois.WhoisClient;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.maven.model.Activation;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Developer;
import org.apache.maven.model.Exclusion;
import org.apache.maven.model.Extension;
import org.apache.maven.model.License;
import org.apache.maven.model.MailingList;
import org.apache.maven.model.Model;
import org.apache.maven.model.Notifier;
import org.apache.maven.model.Profile;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.tomcat.util.codec.binary.Base64;
import org.bouncycastle.gpg.keybox.KeyBlob;
import org.bouncycastle.gpg.keybox.PublicKeyRingBlob;
import org.bouncycastle.gpg.keybox.UserID;
import org.bouncycastle.gpg.keybox.jcajce.JcaKeyBox;
import org.bouncycastle.gpg.keybox.jcajce.JcaKeyBoxBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.egit.github.core.Gist;
import org.eclipse.egit.github.core.GistFile;
import org.eclipse.egit.github.core.service.GistService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.openlocationcode.OpenLocationCode;
import com.google.openlocationcode.OpenLocationCode.CodeArea;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.Message.Status;
import com.twilio.type.PhoneNumber;

import fr.dudie.nominatim.client.JsonNominatimClient;
import fr.dudie.nominatim.client.request.NominatimSearchRequest;
import fr.dudie.nominatim.client.request.paramhelper.PolygonFormat;
import fr.dudie.nominatim.model.Address;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

@Configuration
@Order(1)
@PropertySource("classpath:application.properties")
@RestController
public class Controller implements Filter {
	private static Logger LOG = LoggerFactory.getLogger(Controller.class);

	@Value("${home:76.91.11.245}")
	private String HOME_IP_ADDRESS;

	@Value("${spring.profiles.active:}")
	private String activeProfile;

	@Autowired
	UserRepository userRepository;

	@Autowired
	EndpointRepository endpointRepository;

	public BigDecimal SMS_COST;

	private String TWILIO_ACCOUNT_SID = "AC3044555a5679639f028e046b89b81dc5";

	private String TWILIO_AUTH_TOKEN = "d6234d66c47692149cc46b6c50fd1035";

	@PostConstruct
	public void init() {

		RatesBean.refresh(); // initialize forex rates every time we restart the
		// app
		// add Bouncy JCE Provider, http://bouncycastle.org/latest_releases.html
		Security.addProvider(new BouncyCastleProvider());

		try {
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(this.getClass()
								.getResourceAsStream("/users.csv")));
				CSVParser records = CSVFormat.EXCEL.parse(reader);
				int count = 0;
				for (CSVRecord record : records) {
					User newest = new User();
					newest.setName(record.get(0));
					newest.setNumber(Long.valueOf(record.get(1)));
					this.userRepository.save(newest);
					count++;
				}
				Controller.LOG.info(count + " users imported");
			} catch (NullPointerException e) {
				Controller.LOG.error("No users.csv file found");
			}
			this.endpointRepository
			.save(new Endpoint("/maven", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/reddit", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/icmp", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/plus", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/json", BigDecimal.ZERO));
			this.endpointRepository.save(new Endpoint("/pgp", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/chat", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/whois", BigDecimal.ZERO));
			this.endpointRepository.save(new Endpoint("/bal", BigDecimal.ZERO));
			this.endpointRepository
			.save(new Endpoint("/money", BigDecimal.ZERO));
			this.SMS_COST = new BigDecimal(0.09);
			this.endpointRepository.save(new Endpoint("/sms", this.SMS_COST));
		} catch (DataAccessException e) {
			Controller.LOG.info("data already exists");
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
		}
	}

	public Authenticator buildAuth(String key, String secret) {

		Authenticator ret = null;
		ret = new Authenticator() {
			@Override
			public Request authenticate(Route route, Response response)
					throws IOException {

				User me = Controller.this.userRepository
						.findByAccessKeyAndAccessSecret(key, secret);
				if (me == null) { return null; }
				String auth = Credentials.basic(me.getAccessKey(),
						me.getAccessSecret());
				return response.request().newBuilder()
						.header("Authorization", auth).build();
			}
		};
		return ret;
	}

	@CrossOrigin
	@RequestMapping(value = "/maven/", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> maven(
			@RequestParam Map<String, String> params) {

		final String MAVEN_DEFAULT_SCOPE = "compile";
		Map<String, String> ret = new HashMap<>();
		OkHttpClient client = new OkHttpClient();
		String url = null;
		if (params.containsKey("url")) {
			url = params.get("url");
		} else
			if (params.containsKey("githubuser")) {
				HttpUrl githubUrl = new HttpUrl.Builder()
						.host("raw.githubusercontent.com")
						.addPathSegment(params.get("githubuser"))
						.addPathSegment(params.get("project"))
						.addPathSegment(
								params.getOrDefault("revision", "master"))
						.addPathSegment("pom.xml").build();
				url = githubUrl.toString();
			} else
				if (params.containsKey("bitbucketuser")) {
					HttpUrl bitbucketUrl = new HttpUrl.Builder()
							.host("bitbucket.org").scheme("https")
							.addPathSegment(params.get("bitbucketuser"))
							.addPathSegment(params.get("project"))
							.addPathSegment(
									params.getOrDefault("revision", "master"))
							.addPathSegment("pom.xml").build();
					url = bitbucketUrl.toString();
				} else {
					return new ResponseEntity<>(
							Collections.singletonMap("error",
									"need a url to a maven repository"),
							HttpStatus.PRECONDITION_FAILED);
				}

		HttpUrl httpUrl = HttpUrl.parse(url);
		Request request = new Request.Builder().url(httpUrl).get().build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
			return new ResponseEntity<>(
					Collections.singletonMap("error", e1.getMessage()),
					HttpStatus.NOT_FOUND);
		}

		Model model = null;
		try {
			model = new MavenXpp3Reader().read(response.body().charStream());
		} catch (IOException | XmlPullParserException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(
					Collections.singletonMap("error", e.getMessage()),
					HttpStatus.NOT_ACCEPTABLE);
		}
		ret.put("artifactId", model.getArtifactId());
		if (model.getDescription() != null) {
			ret.put("description", model.getDescription());
		}
		ret.put("groupId", model.getGroupId());
		if (model.getInceptionYear() != null) {
			ret.put("inceptionYear", model.getInceptionYear());
		}
		if (model.getUrl() != null) {
			ret.put("url", model.getUrl());
		}
		ret.put("packaging", model.getPackaging());
		ret.put("version", model.getVersion());

		Build buildInfo = model.getBuild();
		if (buildInfo != null) {
			Map<String, String> build = new HashMap<>();
			if (buildInfo.getOutputDirectory() != null) {
				build.put("outputDirectory", buildInfo.getOutputDirectory());
			}
			if (buildInfo.getScriptSourceDirectory() != null) {
				build.put("scriptSourceDirectory",
						buildInfo.getScriptSourceDirectory());
			}
			if (buildInfo.getDirectory() != null) {
				build.put("directory", buildInfo.getDirectory());
			}
			if (buildInfo.getFinalName() != null) {
				build.put("finalName", buildInfo.getFinalName());
			}
			if (buildInfo.getSourceDirectory() != null) {
				build.put("sourceDirectory", buildInfo.getSourceDirectory());
			}
			if (buildInfo.getTestOutputDirectory() != null) {
				build.put("testOutputDirectory",
						buildInfo.getTestOutputDirectory());
			}
			if (buildInfo.getExtensions() != null) {
				List<Map<String, String>> exts = new ArrayList<>();
				for (Extension extension : buildInfo.getExtensions()) {
					Map<String, String> ext = new HashMap<>();
					ext.put("artifactId", extension.getArtifactId());
					ext.put("groupId", extension.getGroupId());
					ext.put("version", extension.getVersion());
					exts.add(ext);
				}
				build.put("extensions", new Gson().toJson(exts));
			}
			if (!build.isEmpty()) {
				ret.put("build", new Gson().toJson(build));
			}
		}
		if (model.getCiManagement() != null) {
			ret.put("ciManagement:url", model.getCiManagement().getUrl());
			ret.put("ciManagement:system", model.getCiManagement().getSystem());
			List<Map<String, String>> notifiers = new ArrayList<>();
			for (Notifier n : model.getCiManagement().getNotifiers()) {
				Map<String, String> ourNotifier = new HashMap<>();
				ourNotifier.put("address", n.getAddress());
				ourNotifier.put("type", n.getType());
				notifiers.add(ourNotifier);
			}
			ret.put("ciManagement:notifiers", new Gson().toJson(notifiers));

		}

		if (model.getDevelopers() != null) {
			List<Map<String, String>> developers = new ArrayList<>();
			for (Developer d : model.getDevelopers()) {
				Map<String, String> developer = new HashMap<>();
				developer.put("email", d.getEmail());
				developer.put("id", d.getId());
				developer.put("name", d.getName());
				developer.put("url", d.getUrl());
				developer.put("roles", String.join(",", d.getRoles()));
				developer.put("organization", d.getOrganization());
				developer.put("organiationUrl", d.getOrganizationUrl());
				developers.add(developer);
			}
			if (!developers.isEmpty()) {
				ret.put("developers", new Gson().toJson(developers));
			}
		}

		if (model.getLicenses() != null) {
			List<Map<String, String>> licenses = new ArrayList<>();
			for (License l : model.getLicenses()) {
				Map<String, String> license = new HashMap<>();
				license.put("comments", l.getComments());
				license.put("distribution", l.getDistribution());
				license.put("name", l.getName());
				license.put("url", l.getUrl());
				licenses.add(license);
			}
			if (!licenses.isEmpty()) {
				ret.put("licenses", new Gson().toJson(licenses));
			}
		}
		if (model.getMailingLists() != null) {
			List<Map<String, String>> mailingLists = new ArrayList<>();
			for (MailingList m : model.getMailingLists()) {
				Map<String, String> mailingList = new HashMap<>();
				mailingList.put("archive", m.getArchive());
				mailingList.put("name", m.getName());
				mailingList.put("subscribe", m.getSubscribe());
				mailingList.put("unsubscribe", m.getUnsubscribe());
				mailingList.put("post", m.getPost());
				mailingList.put("otherArchives",
						String.join(",", m.getOtherArchives()));

				mailingLists.add(mailingList);
			}
			if (!mailingLists.isEmpty()) {
				ret.put("mailingLists", new Gson().toJson(mailingLists));
			}
		}

		if (model.getProfiles() != null) {
			List<Map<String, String>> profiles = new ArrayList<>();
			for (Profile p : model.getProfiles()) {
				Map<String, String> profile = new HashMap<>();
				profile.put("source", p.getSource());
				profile.put("id", p.getId());
				Activation a = p.getActivation();
				if (a != null) {
					profile.put("activationMissing", a.getFile().getMissing());
					profile.put("activationExisting", a.getFile().getExists());
					profile.put("activationJdk", a.getJdk());
					profile.put("activationOsArch", a.getOs().getArch());
					profile.put("activationOsFamily", a.getOs().getFamily());
					profile.put("activationOsName", a.getOs().getName());
					profile.put("activationOsVersion", a.getOs().getVersion());
					profile.put("activationByDefault",
							Boolean.toString(a.isActiveByDefault()));
				}
				profiles.add(profile);
			}
			if (profiles.isEmpty() == false) {
				ret.put("profiles", new Gson().toJson(profiles));
			}
		}

		if (model.getScm() != null) {
			ret.put("scmConnection", model.getScm().getConnection());
			ret.put("scmDeveloperConnection",
					model.getScm().getDeveloperConnection());
			ret.put("scmTag", model.getScm().getTag());
			ret.put("scmUrl", model.getScm().getUrl());
		}
		if (model.getDependencies() != null) {
			List<Map<String, String>> dependencies = new ArrayList<>();
			for (Dependency d : model.getDependencies()) {
				Map<String, String> dependency = new HashMap<>();
				dependency.put("artifactId", d.getArtifactId());
				dependency.put("groupId", d.getGroupId());
				dependency.put("version", d.getVersion());
				dependency.put("classifier", d.getClassifier());
				dependency.put("optional", d.getOptional());
				if (d.getScope() != "") {
					dependency.put("scope", d.getScope());
				} else {
					dependency.put("scope", MAVEN_DEFAULT_SCOPE);
				}
				if (d.getClassifier() != "") {
					dependency.put("classifier", d.getClassifier());
				}
				if (d.getType() != "") {
					dependency.put("type", d.getType());
				}

				if (d.getExclusions() != null) {
					List<Map<String, String>> exclusions = new ArrayList<>();
					for (Exclusion exclusion : d.getExclusions()) {
						Map<String, String> unitsExclusion = new HashMap<>();
						unitsExclusion.put("artifactId",
								exclusion.getArtifactId());
						unitsExclusion.put("groupId", exclusion.getGroupId());
						exclusions.add(unitsExclusion);
					}
					if (!exclusions.isEmpty()) {
						dependency.put("exclusions",
								new Gson().toJson(exclusions));
					}
				}
				dependencies.add(dependency);

			}
			ret.put("dependencies", new Gson().toJson(dependencies));
		}

		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value = "/icmp/{hostname}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> icmp(
			@RequestParam Map<String, String> params,
			@RequestHeader Map<String, String> headers,
			@PathVariable String hostname) {


		Map<String, String> ret = new HashMap<>();

		HttpUrl.Builder builder = new HttpUrl.Builder().host(hostname);

		if (!params.containsKey("ssl")) {
			params.put("ssl", "prefer");
		}
		if (params.get("ssl").equals("require")
				|| params.get("ssl").equals("prefer")) {
			builder = builder.scheme("https");
			ret.put("ssl", "true");
		} else {
			builder = builder.scheme("http");
			ret.put("ssl", "false");
		}
		ret.put("host", hostname);

		if (params.containsKey("port")) {
			ret.put("port", params.get("port"));
		}

		if (!params.containsKey("path")) {
			params.put("path", "/");
		}

		ret.put("path", params.get("path"));

		OkHttpClient client = new OkHttpClient().newBuilder()
				.followSslRedirects(true).build();

		Request request = new Request.Builder().get().url(builder.build())
				.build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
			if (params.get("ssl").equals("require")) {
				return new ResponseEntity<>(ret, HttpStatus.OK);
			}
		}
		// fallback to http if ssl is not requested
		if (!params.get("ssl").equals("require")) {
			builder = builder.scheme("http");
			request = new Request.Builder().get().url(builder.build()).build();
			try {
				response = client.newCall(request).execute();
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			ret.put("ssl", "false");
		} else {
		}
		ret.put("reachable", Boolean.toString(response.isSuccessful()));
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/icmp", method = RequestMethod.GET)
	public ResponseEntity<String> icmp(@RequestParam Map<String, String> params,
			org.springframework.ui.Model model) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/templates/pingability.html")));
		StringWriter retSource = new StringWriter();
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				retSource.append(line);
			}
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
		}

		String retVal = retSource.toString();
		return ResponseEntity.ok().header("Content-Type", "text/html").body(retVal);
	}

	@CrossOrigin
	@RequestMapping(value = "/reddit", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, String>>> reddit(
			@RequestParam(required = false, defaultValue = "depthhub", name = "sub") String subreddit,
			@RequestParam(defaultValue = "100", name = "limit", required = false) String entryCount) {

		List<Map<String, String>> ret = new ArrayList<>();
		try {
			NumberFormat.getIntegerInstance().parse(entryCount);
		} catch (ParseException e1) {
			Controller.LOG.error(String.format(""), e1);
		}
		HttpUrl url = new HttpUrl.Builder().scheme("https").host("reddit.com")
				.addPathSegment("r").addPathSegment(subreddit)
				.addPathSegment("new.json")
				.addQueryParameter("limit", entryCount).build();
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url(url).build();
		String redditResponse = null;
		try {
			Response response = client.newCall(request).execute();
			redditResponse = response.body().string();
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
		}
		JsonObject responseAsJson = new Gson().fromJson(redditResponse,
				JsonObject.class);
		JsonArray children = responseAsJson.get("data").getAsJsonObject()
				.get("children").getAsJsonArray();
		for (JsonElement baseChild : children) {
			Map<String, String> entry = new HashMap<>();
			JsonObject child = baseChild.getAsJsonObject();
			for (String k : child.getAsJsonObject().keySet()) {
				JsonElement val = child.get(k);
				this.getType(val, k);
				if (val.isJsonObject()) {
					JsonObject array = val.getAsJsonObject();
					for (String i : array.keySet()) {
						JsonElement elem = array.get(i);
						if (elem.isJsonPrimitive()) {
							String newKey = CaseFormat.LOWER_UNDERSCORE
									.to(CaseFormat.LOWER_CAMEL, i);
							entry.put(newKey, elem.getAsString());
						}
					}

				}
			}
			ret.add(entry);
		}

		return new ResponseEntity<>(ret, HttpStatus.OK);

	}

	private String getType(JsonElement val, String k) {

		if (val.isJsonArray()) { return "array"; }
		if (val.isJsonNull()) { return "null"; }
		if (val.isJsonObject()) { return "object"; }
		if (val.isJsonPrimitive()) { return "primitive"; }
		return null;
	}

	@RequestMapping(value = "/money/{amount}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> money(
			@RequestParam Map<String, String> params,
			@PathVariable Double amount) {

		Map<String, String> ret = new HashMap<>();
		ret.put("requestedAmount",
				NumberFormat.getNumberInstance().format(amount));
		ret.put("sourceCurrency", params.get("from").toUpperCase());
		ret.put("destinationCurrency", params.get("to").toUpperCase());
		ret.put("source", "ECB");
		String rateFrom = RatesBean.get(params.get("from"));
		String rateTo = RatesBean.get(params.get("to"));
		if (null == rateFrom) {
			ret.put("error", "from currency not found, valid ones are "
					+ RatesBean.getKeys());
			return new ResponseEntity<>(ret,
					HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
		}
		if (null == rateTo) {
			ret.put("error", "to currency not found, valid ones are "
					+ RatesBean.getKeys());
			return new ResponseEntity<>(ret,
					HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
		}
		DateTimeFormatter fmt = ISODateTimeFormat.date();
		String today = fmt.print(new DateTime());
		ret.put("requestedDate", today);
		Double fromAsEuro = Double
				.valueOf(RatesBean.get(ret.get("sourceCurrency")));
		Double toAsEuro = Double
				.valueOf(RatesBean.get(ret.get("destinationCurrency")));
		Double fromAsTo = Double.valueOf((fromAsEuro / toAsEuro) * amount);
		String finalAmount = NumberFormat.getNumberInstance().format(fromAsTo);

		ret.put("amount", finalAmount);
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/bal", method = RequestMethod.GET)
	public ResponseEntity<Map<String, BigDecimal>> balance(
			@RequestParam(value = "user") String username) {

		Map<String, BigDecimal> ret = new HashMap<>();
		User obj = this.userRepository.findByName(username);
		if (obj == null) {
			ret.put(username + " not found", BigDecimal.valueOf(-999l));
			return new ResponseEntity<>(ret, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		ret.put(username, obj.getMoneyAmount());
		return new ResponseEntity<>(ret, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/geocode", method = RequestMethod.POST)
	public ResponseEntity<List<Map<String, String>>> geocode(
			@RequestBody Map<String, String> body) {

		final String source = "©" + LocalDate.now().getYear()
				+ " OpenStreetMap contributors";
		List<Map<String, String>> ret = new ArrayList<>();
		HttpClientBuilder builder = HttpClientBuilder.create();
		SSLConnectionSocketFactory sslConnectionFactory = null;
		try {
			sslConnectionFactory = new SSLConnectionSocketFactory(
					SSLContext.getDefault(), NoopHostnameVerifier.INSTANCE);
		} catch (NoSuchAlgorithmException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
		}
		builder.setSSLSocketFactory(sslConnectionFactory);
		Registry<ConnectionSocketFactory> registry = RegistryBuilder
				.<ConnectionSocketFactory>create()
				.register("https", sslConnectionFactory).build();
		HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(
				registry);
		builder.setConnectionManager(ccm);
		HttpClient httpClient = builder.build();
		final String baseUrl = "https://nominatim.openstreetmap.org";
		final String email = "hd1+units@jsc.d8u.us";

		JsonNominatimClient nominatimClient = new JsonNominatimClient(baseUrl,
				httpClient, email);
		NominatimSearchRequest request = new NominatimSearchRequest();
		request.setPolygonFormat(PolygonFormat.GEO_JSON);
		if (body.containsKey("addr")) {
			request.setQuery(body.get("addr"));
			request.setAddress(true);
			List<Address> response = null;
			try {
				response = nominatimClient.search(request);
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
				return new ResponseEntity<>(
						Collections.singletonList(Collections
								.singletonMap("error", e.getMessage())),
						HttpStatus.PRECONDITION_FAILED);
			}
			for (Address address : response) {
				String displayName = address.getDisplayName();
				String latitude = Double.valueOf(address.getLatitude())
						.toString();
				String longitude = Double.valueOf(address.getLongitude())
						.toString();

				Map<String, String> addr = new TreeMap<>();
				addr.put("displayName", displayName);
				addr.put("latitude", latitude);
				addr.put("longitude", longitude);
				addr.put("source", source);
				addr.put("osmUrl",
						String.format(
								"https://www.openstreetmap.org/#map=17/%s/%s",
								latitude, longitude));
				addr.put("request", body.get("addr"));
				ret.add(addr);
			}
			return new ResponseEntity<>(ret, HttpStatus.OK);
		}
		if (!body.containsKey("addr")) {
			if (!body.containsKey("lat") || (!body.containsKey("lng"))) {
				return new ResponseEntity<>(
						Collections.singletonList(Collections.singletonMap(
								"error",
								"specify both lat and lng **or** addr")),
						HttpStatus.PRECONDITION_REQUIRED);
			}
			Double longitude = Double.valueOf(body.get("lng"));
			Double latitude = Double.valueOf(body.get("lat"));
			Address addr = null;
			try {
				addr = nominatimClient.getAddress(longitude, latitude);
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			Map<String, String> address = new TreeMap<>();
			address.put("address", addr.getDisplayName());
			String req = "(" + latitude + ", " + longitude + ")";
			address.put("request", req);
			ret.add(address);
			return new ResponseEntity<>(ret, HttpStatus.OK);
		}
		return null;

	}

	@RequestMapping(value = "/ipn", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> paymentIpn(
			@RequestBody Map<String, String> params) {

		Controller.LOG.info(new Gson().toJson(params));
		return new ResponseEntity<>(params, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value = "/plus", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> plusCode(
			@RequestBody Map<String, String> body) {

		Map<String, String> ret = new TreeMap<>();

		if (body.containsKey("code")) {
			CodeArea decoded = new OpenLocationCode(body.get("code")).decode();
			Double radius = Double
					.valueOf(Math.pow(
							(Math.pow(decoded.getLatitudeHeight(), 2)
									+ Math.pow(decoded.getLongitudeWidth(), 2)),
							.5));
			ret.put("request", body.get("code"));
			ret.put("latitude", Double.toString(decoded.getCenterLatitude()));
			ret.put("longitude", Double.toString(decoded.getCenterLongitude()));
			ret.put("radius", Double.toString(radius));
		} else
			if (body.containsKey("location")) {
				ret.put("request", body.get("location"));
				List<Map<String, String>> geocodingResponse = this.geocode(
						Collections.singletonMap("addr", body.get("location")))
						.getBody();
				Map<String, String> geocoded = geocodingResponse.get(0);
				String response = OpenLocationCode.encode(
						Double.valueOf(geocoded.get("latitude")),
						Double.valueOf(geocoded.get("longitude")));
				ret.put("plusCode", response);
			} else
				if (body.containsKey("latitude")
						&& body.containsKey("longitude")) {
					ret.put("latitude", body.get("latitude"));
					ret.put("longitude", body.get("longitude"));
					ret.put("plusCode",
							OpenLocationCode.encode(
									Double.parseDouble(body.get("latitude")),
									Double.parseDouble(body.get("longitude"))));
				} else
					if (body.containsKey("url")) {
						HttpUrl parsing = HttpUrl.parse(body.get("url"));
						String fragment = parsing.fragment();
						String[] location = fragment.split("/");
						String lat = location[1];
						String lng = location[2];
						ret.put("request", body.get("url"));
						String plusCode = OpenLocationCode.encode(
								Double.parseDouble(lat),
								Double.parseDouble(lng));
						ret.put("plusCode", plusCode);

					} else
						if (body.containsKey("addr")) {
							ResponseEntity<List<Map<String, String>>> resp = this
									.geocode(body);
							List<Map<String, String>> response = resp.getBody();
							ret.put("request", body.get("addr"));
							body.remove("addr");
							Map<String, String> first = response.get(0);
							body.putAll(first);
							Map<String, String> recursiveResp = this
									.plusCode(body).getBody();
							String plusCode = recursiveResp.get("plusCode");
							ret.put("plusCode", plusCode);
						} else {
							return new ResponseEntity<>(
									Collections.singletonMap("error",
											"location, latitude/longitude, or code required as body"),
									HttpStatus.PRECONDITION_REQUIRED);
						}
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value = "/json", method = RequestMethod.GET)
	public ResponseEntity<List<List<Map<String, String>>>> html(
			@RequestParam Map<String, String> params) {

		if (!params.containsKey("page")) {
			String message = "no way to figure out what you want if you don't give a page";
			return new ResponseEntity<>(
					Collections.singletonList(Collections.singletonList(
							Collections.singletonMap("error", message))),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		String webpage = params.get("page");
		List<List<Map<String, String>>> retSrc = new ArrayList<>();
		OkHttpClient webClient = new OkHttpClient();
		Request webRequest = null;
		try {
			webRequest = new Request.Builder().url(webpage).build();
		} catch (RuntimeException e) {
			return new ResponseEntity<>(
					Collections.singletonList(
							Collections.singletonList(Collections.singletonMap(
									"error", "invalid url -- " + webpage))),
					HttpStatus.SERVICE_UNAVAILABLE);
		}
		Response response = null;
		try {
			response = webClient.newCall(webRequest).execute();
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(
					Collections.singletonList(Collections.singletonList(
							Collections.singletonMap("error", e.getMessage()))),
					HttpStatus.PRECONDITION_FAILED);
		}
		String html = null;
		try {
			html = response.body().string();
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(
					Collections.singletonList(Collections.singletonList(
							Collections.singletonMap("error", e.getMessage()))),
					HttpStatus.SERVICE_UNAVAILABLE);
		}
		Document doc = Jsoup.parse(html);
		Elements tables = doc.select("table");
		for (Element table : tables) {
			List<Map<String, String>> aTable = new ArrayList<>();
			List<Element> rows = table.select("tr").subList(1,
					table.select("tr").size());
			LinkedHashSet<Element> headers = new LinkedHashSet<>(
					table.select("th"));
			for (Element row : rows) {
				Map<String, String> data = new TreeMap<>();
				ListIterator<Element> headerIterator = Lists
						.newArrayList(headers).listIterator();
				for (int i = 0; i != headers.size(); ++i) {
					String header = headerIterator.next().text();
					String dataElement = row.select("td").get(i).text();
					data.put(header.toLowerCase(), dataElement);
				}
				aTable.add(data);
			}
			retSrc.add(aTable);
		}
		return new ResponseEntity<>(retSrc, HttpStatus.OK);
	}

	@RequestMapping(value = "/pgp", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> retrieveKey(
			@RequestParam Map<String, String> body) {

		Map<String, String> ret = new HashMap<>();
		if (!body.containsKey("key")) {
			ret.put("error",
					"need at least the email address to look for as the \"key\" parameter");
			return new ResponseEntity<>(ret, HttpStatus.PRECONDITION_FAILED);
		}
		ret = PGPUtils.fetchFromKeyserver(body.get("key"));
		ret.put("requestedKey", body.get("key"));
		return new ResponseEntity<>(ret, HttpStatus.FOUND);
	}

	@RequestMapping(value = "/chat", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> newMessage(
			@RequestBody Map<String, String> body) {

		Map<String, String> ret = new HashMap<>();
		if (!body.containsKey("message")) {
			return new ResponseEntity<>(
					Collections.singletonMap("error", "message missing"),
					HttpStatus.PRECONDITION_FAILED);
		}
		if (!body.containsKey("key")) {
			return new ResponseEntity<>(
					Collections.singletonMap("error", "key missing"),
					HttpStatus.PRECONDITION_FAILED);
		}
		if (!body.containsKey("recipient")) {
			return new ResponseEntity<>(
					Collections.singletonMap("error", "recipient missing"),
					HttpStatus.PRECONDITION_FAILED);
		}
		byte[] message = Base64.encodeBase64(body.get("message").getBytes());

		PGPPublicKey pubKey = null;
		// Load public key
		String publicKeyPath = PGPUtils.generatePath();
		try {
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(publicKeyPath));
			if (body.get("key").startsWith("0x")
					&& !body.get("key").contains("@")) {
				body.put("key", body.get("key").replace("0x", ""));
			}
			PGPUtils.KEYSERVER_URL = PGPUtils.KEYSERVER_URL
					.removeAllQueryParameters("key");
			Request retrieveKey = new Request.Builder().get()
					.url(PGPUtils.KEYSERVER_URL
							.addQueryParameter("keyId", body.get("key"))
							.build())
					.build();
			Response responseKey = new OkHttpClient().newCall(retrieveKey)
					.execute();
			String bodyString = responseKey.body().string();
			if (bodyString == "Invalid request!") {
				writer.close();
				new File(publicKeyPath).delete();
				return new ResponseEntity<>(
						Collections.singletonMap("error", bodyString),
						HttpStatus.SERVICE_UNAVAILABLE);
			}
			String publicKey = new Gson().fromJson(bodyString, JsonObject.class)
					.get("publicKeyArmored").getAsString();
			writer.write(publicKey);
			writer.close();
		} catch (IOException e2) {
			Controller.LOG.error(e2.getMessage(), e2);
			return new ResponseEntity<>(
					Collections.singletonMap("error", e2.getMessage()),
					HttpStatus.PRECONDITION_FAILED);
		}
		JcaKeyBox is = null;
		try {
			is = new JcaKeyBoxBuilder().setProvider("BC")
					.build(new FileInputStream(publicKeyPath));
		} catch (NoSuchProviderException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
		} catch (NoSuchAlgorithmException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
		} catch (IOException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
		}
		for (KeyBlob key : is.getKeyBlobs()) {
			for (UserID user : key.getUserIds()) { // Huh?
				if (user.getUserIDAsString().contains(body.get("recipient"))) {
					PublicKeyRingBlob pKeyBlob = (PublicKeyRingBlob) key;
					try {
						pubKey = pKeyBlob.getPGPPublicKeyRing().getPublicKey();
					} catch (IOException e) {
						Controller.LOG.error(e.getMessage(), e);
						return new ResponseEntity<>(
								Collections.singletonMap("error",
										e.getMessage()),
								HttpStatus.FAILED_DEPENDENCY);
					}

					body.put("key", user.getUserIDAsString());
				}
			}
		}

		Controller.LOG.debug(message + " is our message, and "
				+ pubKey.getFingerprint() + " is our key's fingerprint");
		String encryptedMessage = PGPUtils.encryptByteArray(message, pubKey);
		if (null != encryptedMessage) {
			GistFile file = new GistFile();
			file.setContent(encryptedMessage);
			Gist gist = new Gist();
			gist.setDescription(body.get("recipient"));
			gist.setFiles(Collections.singletonMap("message.gpg", file));
			GistService service = new GistService();
			service.getClient().setCredentials("hasandiwan",
					"RjHmSx7cfxPGAM2b");
			try {
				gist = service.createGist(gist);
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			if (null != gist) {
				ret.put("url", gist.getFiles().get("message.gpg").getRawUrl());
			}
		}

		return new ResponseEntity<>(ret, HttpStatus.CREATED);

	}

	@CrossOrigin
	@RequestMapping(value = "/whois/{ip}", method = RequestMethod.GET)
	public ResponseEntity<String> getHostName(
			@PathVariable(required = true) String ip) {

		String retVal = "";
		WhoisClient whois;

		whois = new WhoisClient();

		try {
			whois.connect(WhoisClient.DEFAULT_HOST);
		} catch (SocketException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(ip, HttpStatus.GATEWAY_TIMEOUT);
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>(ip, HttpStatus.GATEWAY_TIMEOUT);
		}
		try {
			String raw = whois.query(ip);

			for (String line : raw.substring(0, raw.indexOf(">>>"))
					.split("\n")) {
				Controller.LOG.debug(line);
				if (line.contains(": ")) {
					retVal += line.trim() + "\n";
				}
			}
		} catch (IOException e) {
			Controller.LOG.error(e.getMessage(), e);
			return new ResponseEntity<>("", HttpStatus.CONTINUE);
		}
		if (retVal.trim().equals("")) {
			return new ResponseEntity<>(ip, HttpStatus.NOT_FOUND);
		}
		retVal = retVal.replace("domain", "");
		Controller.LOG.info("Returning " + retVal);
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(value = "/sms", method = RequestMethod.POST)
	ResponseEntity<String> sendNotification(
			@RequestBody Map<String, String> body) {

		body.put("number", body.get("number").replaceAll("\\D+", ""));

		Controller.LOG.info("number is \"" + body.get("number") + "\"");
		ResponseEntity<String> ret = null;
		String messageContents = this.formatMessage(body);
		Twilio.init(this.TWILIO_ACCOUNT_SID, this.TWILIO_AUTH_TOKEN);
		User u = this.userRepository
				.findByNumber(Long.parseLong(body.get("number")));
		Message message = Message
				.creator(new PhoneNumber("+" + u.getNumber().toString()),
						new PhoneNumber("+15017250604"), messageContents)
				.create();
		if (message.getStatus() == Status.ACCEPTED) {
			this.userRepository.deduct(u,
					this.endpointRepository.findByEndpoint("/sms").getAmount());
			ret = new ResponseEntity<>(message.getSid(), HttpStatus.CREATED);
		}

		u.setMoneyAmount(u.getMoneyAmount().subtract(this.SMS_COST));
		HttpHeaders header = new HttpHeaders();

		if (u.getMoneyAmount().compareTo(BigDecimal.ZERO) < 0) {
			header.set("X-Charge", "success");
			u.setMoneyAmount(u.getMoneyAmount()
					.add(this.SMS_COST.multiply(new BigDecimal(100))));
		}
		header.set("X-Remains", u.getMoneyAmount()
				.divide(this.SMS_COST, RoundingMode.HALF_DOWN).toString());
		ret = ResponseEntity.ok().headers(header).body("");

		return ret;

	}

	@RequestMapping(value = "/sms", method = RequestMethod.GET)
	public String formatMessage(@RequestParam Map<String, String> body) {

		String messageContents = null;
		String lookupKey = body.get("number").replaceAll("\\D", "");
		String incomingText = body.get("message");
		Controller.LOG.info("Looking up user with \"" + lookupKey + "\"");
		User u = this.userRepository.findByNumber(Long.parseLong(lookupKey));
		if (u == null) {
			u = this.userRepository.findByTelegramUserId(body.get("number"));
			if (u == null) { return "error: User not found"; }
		}

		DateTimeFormatter fmt = ISODateTimeFormat.dateHourMinute();
		if (incomingText.startsWith("rem")) {
			Request request = new Request.Builder()
					.url("https://balance.d8u.us/bal?num=" + u.getNumber())
					.head().build();
			String httpHeaders = null;
			try {
				httpHeaders = new OkHttpClient().newCall(request).execute()
						.header("X-Remains", "");
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			body.put("remaining", httpHeaders);

			messageContents = "As of " + fmt.print(new DateTime())
			+ ", you have " + body.get("remaining")
			+ " messages remaining.";
		}
		return messageContents;

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		String authHeader = ((HttpServletRequest) req)
				.getHeader("Authentication");
		if (authHeader != null) {
			String encodedAccessKey = authHeader.split(":")[0];
			String encodedAccessSecret = authHeader.split(":")[1];
			String accessKey = new String(
					Base64.decodeBase64(encodedAccessKey));
			String accessSecret = new String(
					Base64.decodeBase64(encodedAccessSecret));
			User u = this.userRepository
					.findByAccessKeyAndAccessSecret(accessKey, accessSecret);
			if (u != null) {
				URI requestUri = URI.create(request.getRequestURI());
				String endpoint = requestUri.getPath();
				BigDecimal amount = this.endpointRepository
						.findByEndpoint(endpoint).getAmount();
				this.userRepository.deduct(u, amount);
				this.userRepository.save(u);
			}

		}
		chain.doFilter(req, resp);
	}

}