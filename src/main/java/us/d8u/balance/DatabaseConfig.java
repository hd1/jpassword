package us.d8u.balance;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfig {

	@Value("${spring.datasource.hikari.jdbc-url}")
	private String dbUrl;

	@Value("${spring.datasource.hikari.username}")
	private String dbUser;

	@Value("${spring.datasource.hikari.password}")
	private String dbPassword;

	@Bean
	public DataSource dataSource() {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(this.dbUrl);
		config.setUsername(this.dbUser);
		config.setPassword(this.dbPassword);

		return new HikariDataSource(config);
	}
}