package us.d8u.balance;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "endpoints")
public class Endpoint {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	Long endpointId;

	@Column(unique = true)
	String endpoint;

	@Column
	BigDecimal amount;

	public Endpoint() {

		super();
	}

	public Endpoint(String string, BigDecimal bigDecimal) {

		this.setAmount(bigDecimal);
		this.setEndpoint(string);
	}

	public String getEndpoint() {

		return this.endpoint;
	}

	public void setEndpoint(String endpoint) {

		this.endpoint = endpoint;
	}

	public BigDecimal getAmount() {

		return this.amount;
	}

	public void setAmount(BigDecimal amount) {

		this.amount = amount;
	}
}
