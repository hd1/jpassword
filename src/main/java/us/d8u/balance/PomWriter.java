package us.d8u.balance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.google.gson.Gson;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PomWriter {
	private static Logger LOG = LoggerFactory
			.getLogger("us.d8u.balance.PomWriter");

	public static void main(String[] args) {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e2) {
			PomWriter.LOG.error(e2.getMessage(), e2);
		}
		Document doc = null;
		try {
			doc = docBuilder.parse("pom.xml");
		} catch (SAXException e2) {
			PomWriter.LOG.error(e2.getMessage(), e2);
		} catch (IOException e2) {
			PomWriter.LOG.error(e2.getMessage(), e2);
		}

		// Get the version element by tag name directly
		Node staff = doc.getElementsByTagName("version").item(1);

		if ("version".equals(staff.getNodeName())) {
			LocalDate newValue = LocalDate.now();
			String value = newValue.toString().replace('-', '.');
			staff.setTextContent(value);
		}
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e1) {
			PomWriter.LOG.error(e1.getMessage(), e1);
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("pom.xml"));
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			PomWriter.LOG.error(e.getMessage(), e);
		}

		OkHttpClient client = new OkHttpClient();
		Map<String, Object> bodyMap = new HashMap<>();
		Map<String, String> fileCtx = new HashMap<>();
		StringWriter testContents = new StringWriter();
		BufferedReader testReader = null;
		try {
			testReader = new BufferedReader(
					new FileReader("src/test/java/us/d8u/balance/Tests.java"));
		} catch (FileNotFoundException e) {
			PomWriter.LOG.error(e.getMessage(), e);
		}
		String line = null;
		try {
			while ((line = testReader.readLine()) != null) {
				testContents.append(line);
			}
		} catch (IOException e) {
			PomWriter.LOG.error(e.getMessage(), e);
		}
		try {
			testReader.close();
		} catch (IOException e) {
			PomWriter.LOG.error(e.getMessage(), e);
		}
		fileCtx.put("content", testContents.toString());
		fileCtx.put("filename", "Tests.java");
		bodyMap.put("files", fileCtx);
		bodyMap.put("description", "units test code");

		RequestBody body = RequestBody.create(
				MediaType.parse("application/json"),
				new Gson().toJson(bodyMap));
		Request request = new Request.Builder().patch(body)
				.url(new HttpUrl.Builder().scheme("https")
						.host("api.github.com").addPathSegment("gists")
						.addPathSegment("519f6431604bf166677feab56423de9d")
						.build())
				.build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			PomWriter.LOG.error(e.getMessage(), e);
			System.err.print(e.getStackTrace());
		}
		if (!response.isSuccessful()) {
			throw new RuntimeException("Failed to update gist");
		} else {
			PomWriter.LOG.info("updated");
		}
	}

}
