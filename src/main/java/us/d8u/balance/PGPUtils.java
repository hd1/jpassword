package us.d8u.balance;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignature;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.bc.BcPGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBEKeyEncryptionMethodGenerator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PGPUtils {

	private static Logger LOG = LoggerFactory
			.getLogger(us.d8u.balance.PGPUtils.class);

	private static final int BUFFER_SIZE = 1 << 16; // should always be power of
	// 2

	private static final int KEY_FLAGS = 27;

	private static final int[] MASTER_KEY_CERTIFICATION_TYPES = new int[] {
			PGPSignature.POSITIVE_CERTIFICATION,
			PGPSignature.CASUAL_CERTIFICATION, PGPSignature.NO_CERTIFICATION,
			PGPSignature.DEFAULT_CERTIFICATION };

	private static final OkHttpClient client = new OkHttpClient.Builder()
			.followRedirects(true).followSslRedirects(true).build();

	static HttpUrl.Builder KEYSERVER_URL = new HttpUrl.Builder()
			.host("keys.mailvelope.com").port(443).scheme("https")
			.addEncodedPathSegment("api").addEncodedPathSegment("v1")
			.addEncodedPathSegment("key");

	public static Map<String, String> fetchFromKeyserver(String keyId) {

		Map<String, String> ret = new HashMap<>();
		PGPUtils.KEYSERVER_URL.removeAllQueryParameters("email");
		if (keyId.contains("@")) { // search by email
			PGPUtils.KEYSERVER_URL.addQueryParameter("email", keyId);
		} else if (keyId.length() == "febad7ffd041bba1".length()){ // keyId
			PGPUtils.KEYSERVER_URL.addQueryParameter("keyId", keyId);
		} else {
			PGPUtils.KEYSERVER_URL.addQueryParameter("fingerprint", keyId.replaceAll(" ", ""));
		}

		Request keyRequest = new Request.Builder().get()
				.url(PGPUtils.KEYSERVER_URL.build()).build();
		Response keyResponse = null;
		try {
			keyResponse = PGPUtils.client.newCall(keyRequest).execute();
		} catch (IOException e1) {
			PGPUtils.LOG.error(e1.getMessage(), e1);
		}

		String hkpResponse = null;
		try {
			hkpResponse = keyResponse.body().string();
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}
		PGPUtils.LOG.info(hkpResponse);
		Gson gson = new Gson();
		ret.put("request", keyId);
		if (hkpResponse.contains("Key not found")) {
			ret.put("error", hkpResponse);
		} else {
			JsonObject json = gson.fromJson(hkpResponse, JsonObject.class);
			ret.put("key", json.get("publicKeyArmored").getAsString());
			ret.put("keyId", json.get("keyId").getAsString());
			ret.put("fingerprint", json.get("fingerprint").getAsString());
		}
		return ret;
	}

	public static PGPPublicKey readPublicKey(String fileName)
			throws IOException, PGPException {

		InputStream keyIn = new BufferedInputStream(
				new FileInputStream(fileName));
		PGPPublicKey pubKey = PGPUtils.readPublicKey(keyIn);
		keyIn.close();
		return pubKey;
	}

	private static PGPPublicKey readPublicKey(InputStream keyIn) {

		PGPPublicKeyRingCollection pgpPub = null;
		try {
			pgpPub = new PGPPublicKeyRingCollection(
					PGPUtil.getDecoderStream(keyIn),
					new JcaKeyFingerprintCalculator());
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		} catch (PGPException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}

		// we just loop through the collection till we find a key suitable for
		// encryption, in the real
		// world you would probably want to be a bit smarter about this.

		Iterator<PGPPublicKeyRing> keyRingIter = pgpPub.getKeyRings();
		while (keyRingIter.hasNext()) {
			PGPPublicKeyRing keyRing = keyRingIter.next();

			Iterator<PGPPublicKey> keyIter = keyRing.getPublicKeys();
			while (keyIter.hasNext()) {
				PGPPublicKey key = keyIter.next();

				if (key.isEncryptionKey()) { return key; }
			}
		}

		throw new IllegalArgumentException(
				"Can't find encryption key in key ring.");
	}

	public static PGPPrivateKey findSecretKey(PGPSecretKeyRingCollection pgpSec,
			long keyID, char[] pass)
					throws PGPException, NoSuchProviderException {

		PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

		if (pgpSecKey == null) { return null; }

		return pgpSecKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
				.setProvider("BC").build(pass));
	}

	public static List<PGPSecretKey> readSecretKey(InputStream in)
			throws IOException, PGPException {

		List<PGPSecretKey> ret = new ArrayList<>();
		PGPSecretKeyRingCollection keyRingCollection = new PGPSecretKeyRingCollection(
				PGPUtil.getDecoderStream(in),
				new JcaKeyFingerprintCalculator());

		//
		// We just loop through the collection till we find a key suitable for
		// signing.
		// In the real world you would probably want to be a bit smarter about
		// this.
		//
		PGPSecretKey secretKey = null;

		Iterator<PGPSecretKeyRing> rIt = keyRingCollection.getKeyRings();
		while (rIt.next() != null) {
			PGPSecretKeyRing ring = rIt.next();
			Iterator<PGPSecretKey> kIt = ring.getSecretKeys();
			while (kIt.hasNext() && PGPUtils.hasKeyFlags(ring.getPublicKey(),
					KeyFlags.SIGN_DATA)) {
				secretKey = kIt.next();
				ret.add(secretKey);
			}
		}

		return ret;
	}

	/**
	 * decrypt the passed in message stream
	 */
	public static void decryptFile(InputStream in, OutputStream out,
			InputStream keyIn, char[] passwd) throws Exception {

		JcaKeyFingerprintCalculator calculator = new JcaKeyFingerprintCalculator();
		Security.addProvider(new BouncyCastleProvider());
		in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

		PGPObjectFactory pgpF = new PGPObjectFactory(in, calculator);
		PGPEncryptedDataList enc;

		Object o = pgpF.nextObject();
		//
		// the first object might be a PGP marker packet.
		//
		if (o instanceof PGPEncryptedDataList) {
			enc = (PGPEncryptedDataList) o;
		} else {
			enc = (PGPEncryptedDataList) pgpF.nextObject();
		}

		//
		// find the secret key
		//
		@SuppressWarnings("unchecked")
		Iterator<PGPPublicKeyEncryptedData> it = enc.getEncryptedDataObjects();
		PGPSecretKey sKey = null;
		PGPPublicKeyEncryptedData pbe = null;

		while ((sKey == null) && it.hasNext()) {
			pbe = it.next();
			long keyId = pbe.getKeyID();
			sKey = PGPUtils.readSecretKey(in).stream()
					.filter(key -> key.getKeyID() == keyId)
					.collect(Collectors.toList()).get(1);
		}

		if (sKey == null) {
			throw new IllegalArgumentException(
					"Secret key for message not found.");
		}

		InputStream clear = pbe
				.getDataStream(new BcPublicKeyDataDecryptorFactory(
						sKey.extractPrivateKey(null)));

		PGPObjectFactory plainFact = new PGPObjectFactory(clear, calculator);

		Object message = plainFact.nextObject();

		if (message instanceof PGPCompressedData) {
			PGPCompressedData cData = (PGPCompressedData) message;
			PGPObjectFactory pgpFact = new PGPObjectFactory(
					cData.getDataStream(), calculator);

			message = pgpFact.nextObject();
		}

		if (message instanceof PGPLiteralData) {
			PGPLiteralData ld = (PGPLiteralData) message;

			InputStream unc = ld.getInputStream();
			int ch;

			while ((ch = unc.read()) >= 0) {
				out.write(ch);
			}
		} else
			if (message instanceof PGPOnePassSignatureList) {
				throw new PGPException(
						"Encrypted message contains a signed message - not literal data.");
			} else {
				throw new PGPException(
						"Message is not a simple encrypted file - type unknown.");
			}

		if (pbe.isIntegrityProtected()) {
			if (!pbe.verify()) {
				throw new PGPException("Message failed integrity check");
			}
		}
	}

	public static void encryptFile(OutputStream out, String fileName,
			PGPPublicKey encKey, boolean withIntegrityCheck)
					throws IOException, NoSuchProviderException, PGPException {

		Security.addProvider(new BouncyCastleProvider());
		boolean armor = true;
		if (armor) {
			out = new ArmoredOutputStream(out);
		}

		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(
				CompressionAlgorithmTags.ZIP);

		PGPUtil.writeFileToLiteralData(comData.open(bOut),
				PGPLiteralData.BINARY, new File(fileName));

		comData.close();

		BcPGPDataEncryptorBuilder dataEncryptor = new BcPGPDataEncryptorBuilder(
				SymmetricKeyAlgorithmTags.TRIPLE_DES);
		dataEncryptor.setWithIntegrityPacket(withIntegrityCheck);
		dataEncryptor.setSecureRandom(new SecureRandom());

		PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(
				new BcPGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.CAST5)
				.setSecureRandom(new SecureRandom()));
		encryptedDataGenerator
		.addMethod(new JcePublicKeyKeyEncryptionMethodGenerator(encKey)
				.setProvider(new BouncyCastleProvider()));

		byte[] bytes = bOut.toByteArray();
		OutputStream cOut = encryptedDataGenerator.open(out, bytes.length);
		cOut.write(bytes);
		cOut.close();
		out.close();
	}

	public static void signEncryptFile(OutputStream out, String fileName,
			PGPPublicKey publicKey, PGPSecretKey secretKey, String password,
			boolean withIntegrityCheck) throws Exception {

		// Initialize Bouncy Castle security provider
		Provider provider = new BouncyCastleProvider();
		Security.addProvider(provider);

		out = new ArmoredOutputStream(out);

		BcPGPDataEncryptorBuilder dataEncryptor = new BcPGPDataEncryptorBuilder(
				SymmetricKeyAlgorithmTags.TRIPLE_DES);
		dataEncryptor.setWithIntegrityPacket(withIntegrityCheck);
		dataEncryptor.setSecureRandom(new SecureRandom());

		PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(
				new JcePGPDataEncryptorBuilder(SymmetricKeyAlgorithmTags.CAST5)
				.setSecureRandom(new SecureRandom()));

		encryptedDataGenerator.addMethod(
				new JcePBEKeyEncryptionMethodGenerator(password.toCharArray()));

		OutputStream encryptedOut = encryptedDataGenerator.open(out,
				new byte[PGPUtils.BUFFER_SIZE]);

		// Initialize compressed data generator
		PGPCompressedDataGenerator compressedDataGenerator = new PGPCompressedDataGenerator(
				CompressionAlgorithmTags.ZIP);
		OutputStream compressedOut = compressedDataGenerator.open(encryptedOut,
				new byte[PGPUtils.BUFFER_SIZE]);

		// Initialize signature generator
		PGPPrivateKey privateKey = secretKey
				.extractPrivateKey(new BcPBESecretKeyDecryptorBuilder(
						new BcPGPDigestCalculatorProvider())
						.build(password.toCharArray()));

		new BcPGPContentSignerBuilder(secretKey.getPublicKey().getAlgorithm(),
				HashAlgorithmTags.SHA1);

		PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(
				new BcPGPContentSignerBuilder(
						PublicKeyAlgorithmTags.RSA_GENERAL,
						HashAlgorithmTags.SHA1));
		signatureGenerator.init(PGPSignature.BINARY_DOCUMENT, privateKey);

		boolean firstTime = true;
		Iterator<String> it = secretKey.getPublicKey().getUserIDs();
		while (it.hasNext() && firstTime) {
			PGPSignatureSubpacketGenerator spGen = new PGPSignatureSubpacketGenerator();
			spGen.setSignerUserID(false, it.next());
			signatureGenerator.setHashedSubpackets(spGen.generate());
			// Exit the loop after the first iteration
			firstTime = false;
		}
		signatureGenerator.generateOnePassVersion(false).encode(compressedOut);

		// Initialize literal data generator
		PGPLiteralDataGenerator literalDataGenerator = new PGPLiteralDataGenerator();
		OutputStream literalOut = literalDataGenerator.open(compressedOut,
				PGPLiteralData.BINARY, fileName, new Date(),
				new byte[PGPUtils.BUFFER_SIZE]);

		// Main loop - read the "in" stream, compress, encrypt and write to the
		// "out" stream
		FileInputStream in = new FileInputStream(fileName);
		byte[] buf = new byte[PGPUtils.BUFFER_SIZE];
		int len;
		while ((len = in.read(buf)) > 0) {
			literalOut.write(buf, 0, len);
			signatureGenerator.update(buf, 0, len);
		}

		in.close();
		literalDataGenerator.close();
		// Generate the signature, compress, encrypt and write to the "out"
		// stream
		signatureGenerator.generate().encode(compressedOut);
		compressedDataGenerator.close();
		encryptedDataGenerator.close();
		out.close();
	}

	public static boolean verifyFile(InputStream in, InputStream keyIn,
			String extractContentFile) throws Exception {

		in = PGPUtil.getDecoderStream(in);

		PGPObjectFactory pgpFact = new PGPObjectFactory(in,
				new JcaKeyFingerprintCalculator());
		PGPCompressedData c1 = (PGPCompressedData) pgpFact.nextObject();

		pgpFact = new PGPObjectFactory(c1.getDataStream(),
				new JcaKeyFingerprintCalculator());

		PGPOnePassSignatureList p1 = (PGPOnePassSignatureList) pgpFact
				.nextObject();

		PGPOnePassSignature ops = p1.get(0);

		PGPLiteralData p2 = (PGPLiteralData) pgpFact.nextObject();

		InputStream dIn = p2.getInputStream();

		IOUtils.copy(dIn, new FileOutputStream(extractContentFile));

		int ch;
		PGPPublicKeyRingCollection pgpRing = new PGPPublicKeyRingCollection(
				PGPUtil.getDecoderStream(keyIn),
				new JcaKeyFingerprintCalculator());

		PGPPublicKey key = pgpRing.getPublicKey(ops.getKeyID());

		FileOutputStream out = new FileOutputStream(p2.getFileName());

		ops.init(new BcPGPContentVerifierBuilderProvider(), key);

		while ((ch = dIn.read()) >= 0) {
			ops.update((byte) ch);
			out.write(ch);
		}

		out.close();

		PGPSignatureList p3 = (PGPSignatureList) pgpFact.nextObject();
		return ops.verify(p3.get(0));
	}

	/**
	 * From LockBox Lobs PGP Encryption tools.
	 * http://www.lockboxlabs.org/content/downloads
	 * I didn't think it was worth having to import a 4meg lib for three methods
	 *
	 * @param key
	 * @return
	 */
	public static boolean isForEncryption(PGPPublicKey key) {

		if ((key.getAlgorithm() == PublicKeyAlgorithmTags.RSA_SIGN)
				|| (key.getAlgorithm() == PublicKeyAlgorithmTags.DSA)
				|| (key.getAlgorithm() == PublicKeyAlgorithmTags.ECDSA)) {
			return false;
		}

		return PGPUtils.hasKeyFlags(key,
				KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);
	}

	/**
	 * From LockBox Lobs PGP Encryption tools.
	 * http://www.lockboxlabs.org/content/downloads
	 * I didn't think it was worth having to import a 4meg lib for three methods
	 *
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean hasKeyFlags(PGPPublicKey encKey, int keyUsage) {

		if (encKey.isMasterKey()) {
			for (int i = 0; i != PGPUtils.MASTER_KEY_CERTIFICATION_TYPES.length; i++) {
				for (Iterator<PGPSignature> eIt = encKey.getSignaturesOfType(
						PGPUtils.MASTER_KEY_CERTIFICATION_TYPES[i]); eIt
						.hasNext();) {
					PGPSignature sig = eIt.next();
					if (!PGPUtils.isMatchingUsage(sig, keyUsage)) {
						return false;
					}
				}
			}
		} else {
			for (Iterator<PGPSignature> eIt = encKey.getSignaturesOfType(
					PGPSignature.SUBKEY_BINDING); eIt.hasNext();) {
				PGPSignature sig = eIt.next();
				if (!PGPUtils.isMatchingUsage(sig, keyUsage)) { return false; }
			}
		}
		return true;
	}

	/**
	 * From LockBox Lobs PGP Encryption tools.
	 * http://www.lockboxlabs.org/content/downloads
	 * I didn't think it was worth having to import a 4meg lib for three methods
	 *
	 * @param key
	 * @return
	 */
	private static boolean isMatchingUsage(PGPSignature sig, int keyUsage) {

		if (sig.hasSubpackets()) {
			PGPSignatureSubpacketVector sv = sig.getHashedSubPackets();
			if (sv.hasSubpacket(PGPUtils.KEY_FLAGS)) {
				// code fix suggested by kzt (see comments)
				if (sv.getKeyFlags() == 0) { return false; }
			}
		}
		return true;
	}

	public static String signMessageByteArray(String message,
			PGPSecretKey pgpSec, char[] charArray)
					throws NoSuchAlgorithmException, PGPException {

		byte[] dataBytes = message.getBytes();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		new BouncyCastleProvider();
		PGPSignatureGenerator sGen = new PGPSignatureGenerator(
				new BcPGPContentSignerBuilder(PublicKeyAlgorithmTags.DSA,
						HashAlgorithmTags.SHA256));
		PGPSignatureSubpacketGenerator spGen = new PGPSignatureSubpacketGenerator();
		PGPPrivateKey prKey = pgpSec
				.extractPrivateKey(new BcPBESecretKeyDecryptorBuilder(
						new BcPGPDigestCalculatorProvider()).build(charArray));
		sGen.init(PGPSignature.CANONICAL_TEXT_DOCUMENT, prKey);
		Iterator<String> userIDs = pgpSec.getPublicKey().getUserIDs();
		if (userIDs.hasNext()) {
			spGen.setSignerUserID(false, userIDs.next());
			sGen.setHashedSubpackets(spGen.generate());
		}

		ArmoredOutputStream aos = new ArmoredOutputStream(baos);
		try {
			aos.beginClearText(HashAlgorithmTags.SHA256);
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}

		sGen.update(dataBytes);
		try {
			aos.write(dataBytes);
			aos.endClearText();
			aos.flush();
			aos.close();
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}

		BCPGOutputStream bOut = new BCPGOutputStream(aos);
		try {
			sGen.generate().encode(bOut);
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}

		return null;
	}

	public static String encryptByteArray(byte[] messageBytes,
			PGPPublicKey pubKey) {

		File temporaryFile = null;
		// encrypt bytes
		String value = "";
		AsymmetricKeyParameter publicKey = null;
		try {
			publicKey = PublicKeyFactory
					.createKey(pubKey.getRawUserIDs().next());
		} catch (IOException e2) {
			PGPUtils.LOG.error(e2.getMessage(), e2);
		}
		AsymmetricBlockCipher cipher = new RSAEngine();
		PKCS1Encoding encoding = new PKCS1Encoding(cipher);
		encoding.init(true, publicKey);
		int i = 0;
		int len = encoding.getInputBlockSize();
		while (i < messageBytes.length) {
			if ((i + len) > messageBytes.length) {
				len = messageBytes.length - i;
			}

			byte[] hexEncodedCipher = null;
			try {
				hexEncodedCipher = encoding.processBlock(messageBytes, i, len);
			} catch (InvalidCipherTextException e) {
				PGPUtils.LOG.error(e.getMessage(), e);
			}
			value = value + PGPUtils.getHexString(hexEncodedCipher);
			i += encoding.getInputBlockSize();
		}
		PGPUtils.LOG.debug("encrypted message is " + new String(messageBytes));

		try {
			temporaryFile = File.createTempFile(
					new String(messageBytes).split(" ")[0], ".pgp");
		} catch (IOException e1) {
			PGPUtils.LOG.error(e1.getMessage(), e1);
		}
		try {
			FileUtils.writeStringToFile(temporaryFile, value, "utf-8");
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}
		String ret = null;
		try {
			ret = FileUtils.readFileToString(temporaryFile,
					Charset.defaultCharset());
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}
		temporaryFile.delete();
		return ret;
	}

	private static String getHexString(byte[] hexEncodedCipher) {

		String result = "";
		for (byte element : hexEncodedCipher) {
			result += Integer.toString((element & 0xff) + 0x100, 16)
					.substring(1);
		}
		return result;
	}

	public static String generatePath() {

		File f = null;
		try {
			f = File.createTempFile("test", ".gpg");
		} catch (IOException e) {
			PGPUtils.LOG.error(e.getMessage(), e);
		}
		return f.getAbsolutePath();
	}

}
