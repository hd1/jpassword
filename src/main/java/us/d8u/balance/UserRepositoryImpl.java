package us.d8u.balance;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import okhttp3.OkHttpClient;

@Qualifier("UserRepositoryImpl")
public class UserRepositoryImpl implements CustomUserRepository {
	private static final Logger LOG = LoggerFactory
			.getLogger(UserRepositoryImpl.class);

	@Autowired
	UserRepository repo;

	final static Integer TIMEOUT = 30;

	static OkHttpClient client = new OkHttpClient.Builder()
			.connectTimeout(UserRepositoryImpl.TIMEOUT, TimeUnit.SECONDS)
			.writeTimeout(UserRepositoryImpl.TIMEOUT, TimeUnit.SECONDS)
			.readTimeout(UserRepositoryImpl.TIMEOUT, TimeUnit.SECONDS).build();

	@Override
	public void deduct(User u, BigDecimal amount) {

		UserRepositoryImpl.LOG
		.info("incoming user in deduct is " + u.getNumber());

		u.setMoneyAmount(
				u.getMoneyAmount().subtract(amount));
	}

}
