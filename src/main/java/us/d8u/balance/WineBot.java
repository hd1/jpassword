package us.d8u.balance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class WineBot extends TelegramLongPollingBot {
	private static Logger LOG = LoggerFactory
			.getLogger(us.d8u.balance.WineBot.class);

	@Override
	public String getBotUsername() {

		return "wine90210_bot";
	}

	@Override
	public void onUpdateReceived(Update update) {

		WineBot.LOG.info("message received " + update.getMessage().getText());
		if (update.hasMessage() && update.getMessage().hasText()) {
			SendMessage message = new SendMessage()
					.setChatId(update.getMessage().getChatId())
					.setText(update.getMessage().getText());
			try {
				this.execute(message);
			} catch (TelegramApiException e) {
				WineBot.LOG.error(e.getMessage(), e);
			}
		}

	}

	@Override
	public String getBotToken() {

		return "1062997935:AAEdkrSxzg5KYUxvFMQv02kZ39Y19CmoMMc";
	}

}
