package us.d8u.balance;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RatesBean {
	private static Logger LOG = LoggerFactory.getLogger(RatesBean.class);

	private static Map<String, String> wrapped = new HashMap<>();

	public void put(String k, String v) {

		RatesBean.wrapped.put(k, v);
	}

	public static String get(String k) {

		return RatesBean.wrapped.get(k);
	}

	public static Boolean empty() {

		return RatesBean.wrapped.isEmpty();
	}

	@Scheduled(cron = "0 0 17 * * MON-FRI", zone = "Europe/Berlin")
	public static void refresh() {

		RatesBean.LOG.info("in refresh, at " + new DateTime());
		Map<String, String> map = new HashMap<>();
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url(
				"https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
				.build();
		Response resp = null;
		try {
			resp = client.newCall(request).execute();
		} catch (IOException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		}

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = null;
		try {
			saxParser = factory.newSAXParser();
		} catch (ParserConfigurationException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		} catch (SAXException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		}
		DefaultHandler handler = new DefaultHandler() {

			@Override
			public void startElement(String uri, String localName, String qName,
					Attributes attributes) throws SAXException {

				if (qName.equals("Cube")
						&& (attributes.getValue("currency") != null)) {
					map.put(attributes.getValue("currency"),
							attributes.getValue("rate"));
				}
			}
		};
		try {
			saxParser.parse(resp.body().byteStream(), handler);
		} catch (SAXException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		}
		RatesBean.wrapped = map;
		RatesBean.LOG.debug(new Gson().toJson(map));
	}

	public static String getKeys() {

		StringWriter ret = new StringWriter();
		try (CSVPrinter p = new CSVPrinter(ret, CSVFormat.DEFAULT)) {
			p.printRecord(RatesBean.wrapped.keySet());
		} catch (IOException e) {
			RatesBean.LOG.error(e.getMessage(), e);
		}
		return "{" + ret.toString() + "}";
	}

}
