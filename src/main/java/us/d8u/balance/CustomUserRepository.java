package us.d8u.balance;

import java.math.BigDecimal;

public interface CustomUserRepository {
	void deduct(User u, BigDecimal amount);
}