package us.d8u.balance;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@SpringBootApplication
public class Application implements WebMvcConfigurer {
	@SuppressWarnings("unused")
	private static Logger LOG = LoggerFactory
	.getLogger(us.d8u.balance.Application.class);

	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {

		// set path extension to true
		configurer.favorPathExtension(true).
		// set favor parameter to false
		favorParameter(false).
		// ignore the accept headers
		ignoreAcceptHeader(true).
		// don't use Java Activation Framework since we are manually
		// specifying the media types required below
		useRegisteredExtensionsOnly(true)
		.defaultContentType(new MediaType("application", "json",
				Charset.forName("utf-8")))
		.mediaType("csv",
				new MediaType("text", "csv", Charset.forName("utf-8")))
		.mediaType("json", new MediaType("application", "json",
				Charset.forName("utf-8")));
	}

	public static void main(String[] args) {
		RatesBean.refresh();
		SpringApplication.run(Application.class, args);
	}

}
